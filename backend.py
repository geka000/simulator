from bottle import Bottle, run
from core.World import World
import json

w = World()


w.tick()
exit()
app = Bottle()

@app.route('/tick')
def tick():
    w.tick()
    return "{'result':1}"

@app.route('/state')
def get_state():
    objects = []
    for o in w.objects:
        objects.append({
            'name': o.name,
            'x': o.x,
            'y': o.y
        })
    result = {'result': 1, 'objects': objects}
    return json.dumps(result)

run(app, host='localhost', port=8080)



