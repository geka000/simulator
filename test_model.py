from Model import Model
from core.World import World
from core.Actor import Actor
from core.Math import  point_length, vec_length, vec_to, vec_mul, vec_acc, vec_norm
from simulator import Simulator


class SimpleActor(Actor):
    def __init__(self, name, x, y, vision, speed):
        super().__init__(name, x, y, vision)
        self.speed = speed

    def move(self, direction_vector):
        vec = vec_norm(direction_vector)

        def m():
            self.x += vec[0]*self.speed
            self.y += vec[1]*self.speed

        return m

    def vector_to(self, obj):
        p0 = (self.x, self.y)
        p1 = (obj.x, obj.y)
        v = vec_to(p0, p1)
        return v


class Rabbit(SimpleActor):
    def __init__(self, x, y):
        super().__init__("rabbit", x, y, 50, 5)

    def compute(self, world):
        objects = world.get_vision(self)
        vec_to_danger = []
        for obj in objects:
            if obj.name == 'wolf':
                #print(obj._to_json())
                v = self.vector_to(obj)
                vec_to_danger.append(v)
                #print("danger", vec_to_danger)
        acc_danger = (0, 0)
        for d in vec_to_danger:
            acc_danger = vec_acc(acc_danger, d)
        if acc_danger == (0, 0):
            pass
        else:
            acc_danger_inverse = vec_mul(acc_danger, -1)
            #print("move_to", move_vec)
            return self.move(acc_danger_inverse)
        return None


class Wolf(SimpleActor):
    def __init__(self, x, y):
        super().__init__("wolf", x, y, 20, 6)

    def compute(self, world):
        objects = world.get_vision(self)
        nearest = None
        nearest_len = self.vision
        for obj in objects:
            if obj.name == 'rabbit':
                if world.get_intersections(self, obj):
                    print ("kill")
                p = (obj.x, obj.y)
                if nearest is None:
                    nearest = obj
                else:
                    l = point_length((self.x, self.y), p)
                    if l < nearest_len:
                        nearest_len = l
                        nearest = obj
        if nearest is not None:
            v = self.vector_to(nearest)
            return self.move(v)
        pass


class TestModel(Model):
    def create(self, world):
        a = Rabbit(0, 0)
        world.place_actor(a)
        b = Wolf(10, 10)
        world.place_actor(b)
        b = Wolf(-10, 10)
        world.place_actor(b)

m = TestModel()

w = World()
m.create(w)

for i in range(10):
    for o in w.objects:
        print(o._to_json())
    w.tick()
    print("---")

#simulator = Simulator(m)
#simulator.start()

