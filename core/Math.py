import math


def point_length(p1, p2):
    return vec_length((p2[0]-p1[0], p2[1]-p1[1]))


def vec_length(v):
    return math.sqrt(math.pow(v[0], 2) + math.pow(v[1], 2))


def vec_to(p1, p2):
    return p2[0] - p1[0], p2[1] - p1[1]


def vec_mul(vec, scalar):
    return vec[0]* scalar, vec[1]*scalar


def vec_norm(vec):
    return vec_mul(vec, 1.0/vec_length(vec))


def vec_acc(v1, v2):
    return v1[0] + v2[0], v1[1] + v2[1]