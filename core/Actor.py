from core.WorldObject import WorldObject
from abc import abstractclassmethod


class Actor(WorldObject):
    def __init__(self, name, x, y, vision):
        super().__init__(name, x, y)
        self.vision = vision
        self.actions = {}

    def tick(self, world):
        action = self.compute(world)
        if action is not None:
            action()

    @abstractclassmethod
    def compute(self, world):
        pass
