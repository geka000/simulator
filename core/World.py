from core.Math import point_length


class World:
    def __init__(self):
        self.objects = []
        self.actors = []

    def place_object(self, object):
        self.objects.append(object)

    def place_actor(self, actor):
        self.objects.append(actor)
        self.actors.append(actor)

    def get_vision(self, actor):
        result = []
        x = actor.x
        y = actor.y
        r = actor.vision
        for o in self.objects:
            if point_length((x, y), (o.x, o.y)) <= r and o is not actor:
                result.append(o)
        return result

    @staticmethod
    def get_intersections(o1, o2):
        return point_length((o1.x, o1.y), (o2.x, o2.y)) <= 10

    def tick(self):
        for a in self.actors:
            a.tick(self)



