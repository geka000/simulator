

class WorldObject:
    def __init__(self, name, x, y):
        self.name = name
        self.x = x
        self.y = y

    def _to_json(self):
        return {'name': self.name, 'x': self.x, 'y': self.y}
