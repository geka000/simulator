from core.World import World
from core.Actor import Actor

class SimpleActor(Actor):
    def __init__(self, name, x, y, vision, speed):
        super().__init__(name, x, y, vision)
        self.speed = speed
        self.actions = {
            "up": self.move_up,
            "down": self.move_down,
            "left": self.move_left,
            "right": self.move_right
        }

    def move_up(self):
        self.x += self.speed

    def move_down(self):
        self.x -= self.speed

    def move_left(self):
        self.y -= self.speed

    def move_right(self):
        self.y += self.speed


class A(SimpleActor):
    def __init__(self, x, y):
        super().__init__("a", x, y, 50, 5)

    def compute(self, world):
        print( world.get_vision(self) )
        return None


class B(SimpleActor):
    def __init__(self, x, y):
        super().__init__("b", x, y, 100, 10)

    def compute(self, world):
        return "right"


w = World()
a = A(0, 0)
w.place_actor(a)
b = B(10, 10)
w.place_actor(b)

for i in range(10):
    w.tick()

