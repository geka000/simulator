from abc import abstractmethod

class Model:
    def __init__(self):
        pass

    @abstractmethod
    def create(self, world):
        pass
