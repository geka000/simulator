from bottle import Bottle, run
from core.World import World
import json


def methodroute(route):
    def decorator(f):
        f.route = route
        return f
    return decorator


class Simulator:
    def __init__(self, model, world=None):
        if world is None:
            self.world = World()
        else:
            self.world = world
        model.create(self.world)
        self._app = Bottle()
        self._route()

    def _route(self):
        self._app.route('/tick',  callback=self._tick)
        self._app.route('/state', callback=self._state)

    def _tick(self):
        self.world.tick()
        return "{'result':1}"

    def _state(self):
        objects = []
        for o in self.world.objects:
            objects.append(o._to_json())
        result = {'result': 1, 'objects': objects}
        return json.dumps(result)

    def start(self):
        self._app.run(host='localhost', port=8080)



